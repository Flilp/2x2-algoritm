# Algoritm

Detta är en algoritm för att beräkna respektive attenueringsvärde i en 2x2-matris. Denna algoritm simulerar en DT, men istället för att skanna tredimensionella voxlar och projicera dem på tvådimensionella matriser så projiceras en tvådimensionell matris på en endimensionell lista.
